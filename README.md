# How to use
- export "Daily Kanji" deck to input/vocabulary.txt
- export current kanji deck to input/current.txt


# TO DO
- How to update card order. If later I am adding more kanji, how do I update the order. Keep card already started but card not started yet can be reordered


- Note entry
    * Kanji
    * Meaning
    * Vocabulary
    * Note



Workflow:
- Je veux pouvoir garder mes notes
- Je veux pouvoir rajouter des kanjis après coup
- Je veux pouvoir ajourter des kanjis à la main


1. Merge db from anki deck export
2. Add new kanji from "Daily Japanese" deck