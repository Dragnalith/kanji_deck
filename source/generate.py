import os
import json
from kanji_info import kanji_all, kanji_joyo, joyo_index
import genanki

global root_path
global vocabulary_path
global current_path
global output_deck_path

root_path = os.path.realpath(__file__)
root_path = os.path.abspath(os.path.join(os.path.dirname(root_path), os.pardir))
vocabulary_path = os.path.join(root_path, "input/vocabulary.txt")
current_path = os.path.join(root_path, "input/current.txt")
db_path = os.path.join(root_path, "data/kanji_db.json")
meaning_path = os.path.join(root_path, "data/meanings.txt")
output_deck_path = os.path.join(root_path, "output/voc_kanji.apkg")

global note_type_id
global deck_id
global card_template_find
global card_template_read
global card_template_answer

note_type_id = 1482182645
deck_id = 2086548955

card_template_header_read =""" 
        <div class="kanji_card">
            <p class="kanji">{{Kanji}}</p>
            <p class="meaning invisible">{{Meaning}}</p>
            <ul id="kanji_voc">
            </ul>
        </div>
"""
card_template_header_find ="""
        <div class="meaning_card">
            <p class="kanji invisible">{{Kanji}}</p>
            <p class="meaning">{{Meaning}}</p>
            <ul id="meaning_voc">
            </ul>
        </div>
"""
card_template_header_answer = """
        <div class="answer_card">
            <p class="kanji">{{Kanji}}</p>
            <p class="meaning">{{Meaning}}</p>
            <ul id="answer_voc">
            </ul>
            {{#Note}}
            <div class="note">Note: {{Note}}</div>
            {{/Note}}
            <div class="kanji stroke">{{Kanji}}</div>
        </div>
"""

card_template = """
        <div id="vocabulary" class="hidden">{{ Vocabulary }}</div>
        <div id="kanji_char" class="hidden">{{ Kanji }}</div>
        <script "text/javascript">
            var parseVocabulary = function(vocabulary) {
                var voc_entry = vocabulary.split(";");
                var words = [];
                for (i = 0; i < voc_entry.length; i++) {
                    var s = voc_entry[i].split('=');
                    if (s.length === 3)
                    {
                        words.push(s);
                    }
                }

                return words;
            };
            var generateKanjiNormal = function(text, kanji) {
                var kanji_voc = document.createElement("span");
                kanji_voc.className = "kanji_voc"
                kanji_voc.textContent = text;

                return kanji_voc;
            }
            var generateKanjiSquare = function(text, kanji) {
                var kanji_voc = document.createElement("span");
                kanji_voc.className = "kanji_voc"
                kanji_voc.textContent = text.replace(kanji, "〼");

                return kanji_voc;
            }
            var generateKanjiBold = function(text, kanji) {
                var i = text.indexOf(kanji);
                var start = text.substring(0, i);
                var end = text.substring(i+1);
                var kanji_voc = document.createElement("span");
                var start_span = document.createElement("span");
                start_span.textContent = start;
                var kanji_span = document.createElement("span");
                kanji_span.textContent = kanji;
                kanji_span.className = "kanjiBold";
                var end_span = document.createElement("span");
                end_span.textContent = end;
                kanji_voc.className = "kanji_voc";
                kanji_voc.appendChild(start_span);
                kanji_voc.appendChild(kanji_span);
                kanji_voc.appendChild(end_span);

                return kanji_voc;
            }
            var generateList = function(cls, words, kanji, generator, showFurigana) {
                var el = document.getElementById(cls);
                if (el === null)
                {
                    return;
                }
                for (i = 0; i < words.length; i++)
                {
                    var item = document.createElement("li")
                    var kanji_voc = generator(words[i][0], kanji);
                    var arrow = document.createElement("span");
                    arrow.textContent = " → ";
                    arrow.className = "arrow";
                    var furigana = document.createElement("span");
                    furigana.textContent = words[i][1];
                    furigana.className = "furigana";
                    var translation = document.createElement("span");
                    translation.textContent =  "　(" + words[i][2] + ")";
                    item.appendChild(kanji_voc);
                    if (showFurigana)
                    {
                        item.appendChild(arrow);
                        item.appendChild(furigana);
                    }
                        item.appendChild(translation)
                    el.appendChild(item);
                }
            };
            (function() {
                var vocabulary = document.getElementById("vocabulary").textContent;
                var kanji = document.getElementById("kanji_char").textContent;
                var words = parseVocabulary(vocabulary);
                generateList("kanji_voc", words, kanji, generateKanjiBold, false);
                generateList("meaning_voc", words, kanji, generateKanjiSquare, true);
                generateList("answer_voc", words, kanji, generateKanjiNormal, true);
            })();
        </script>
"""

card_template_read = card_template_header_read + card_template
card_template_find = card_template_header_find + card_template
card_template_answer = card_template_header_answer + card_template

card_template_css = """
body {
    font-size: 19px;
}

.note {
    font-style: italic;
    font-size: 16px;
}

.invisible {
    visibility: hidden;
}

.kanji {
    font-family: "MS Mincho";
    text-align: center;
    font-size: 100px;
    margin: 0px;
}

.kanji.stroke {
    font-family: "KanjiStrokeOrders";
    font-size: 140px;
}

.furigana {
    font-family: "MS Mincho";
}

.kanjiBold {
    font-weight: bolder;
}

.kanji_voc {
    font-family: "MS Mincho";
}

.arrow {
    margin-left: 10px;
    margin-right: 10px;
}

.hidden {
    display:none;
}

.meaning {
    text-align: center;
    font-size: 22px;
    font-weight: bold;
}
"""

def cleanDoubleQuote(s):
    value = s.strip()
    if value[0] == '\"' and value[0] == '\"':
        value = value[1:-1]
        value = value.replace("\"\"", "\"")
    value = value.strip()
    return value

class VocabularyEntry:
    def __init__(self, kanji, furigana, translation):
        self.kanji = kanji.strip()
        self.furigana = furigana.strip()
        # Anki encode double quote weirdly
        self.translation = cleanDoubleQuote(translation)

    @classmethod
    def from_json(cls, data):
        return cls(data["kanji"].strip(), data["furigana"].strip(), data["translation"].strip())

    @classmethod
    def from_line(cls, line):
        items = line.split('=')
        if len(items)!=3:
            print("vocabulary parse error: {}".format(line))
            return None
        return cls(items[0].strip(), items[1].strip(), items[2].strip())

    def to_json(self):
        data = {}
        data["kanji"] = self.kanji.strip()
        data["furigana"] = self.furigana.strip()
        data["translation"] = self.translation.strip()

        return data

    def to_line(self):
        return "" + self.kanji + "=" + self.furigana + "=" + self.translation

class KanjiNote(genanki.Note):
    @property
    def guid(self):
      return genanki.guid_for(self.fields[0])

    #@property
    #def sort_field(self):
    #    return self.fields[0]

class KanjiEntry:
    def __init__(self, kanji):
        self.kanji = kanji
        if kanji in joyo_index:
            self.joyo_index = joyo_index[kanji]
        else:
            self.joyo_index = len(kanji_joyo)
        self.meaning = ""
        self.vocabulary = {}
        self.note = ""

    @classmethod
    def from_json(cls, data):
        entry = KanjiEntry(data["kanji"])
        entry.meaning = data["meaning"].strip()
        entry.note = data["note"].strip()
        entry.vocabulary = {x["kanji"]: VocabularyEntry.from_json(x) for k, x in data["vocabulary"].items()}

        return entry
    
    @classmethod
    def from_line(cls, line):
        items = line.split('\t')
        assert len(items) >= 4
        items[2] = cleanDoubleQuote(items[2])
        entry = cls(items[0].strip())
        entry.meaning = cleanDoubleQuote(items[1])
        entry.note = items[3].strip()
        entry.vocabulary = {VocabularyEntry.from_line(x).kanji: VocabularyEntry.from_line(x) for x in items[2].split(';') if VocabularyEntry.from_line(x) is not None}

        return entry

    def to_json(self):
        data = {}
        data["kanji"] = self.kanji.strip()
        data["meaning"] = self.meaning.strip()
        data["note"] = self.note.strip()
        data["vocabulary"] = {k: x.to_json() for k, x in self.vocabulary.items()}

        return data


def import_vocabulary():
    notes = []
    with open(vocabulary_path, 'r') as file:
        for line in file:
            items = line.split('\t')
            notes.append(VocabularyEntry(items[0], items[1], items[2]))

    return notes

def import_current():
    db = {}
    with open(current_path, 'r') as file:
        for line in file:
            entry = KanjiEntry.from_line(line)
            assert entry.kanji not in db
            db[entry.kanji] = entry

    return db

def import_db():
    with open(db_path) as db_file:  
        data = json.load(db_file)

        db = {}
        for kanji, entry_data in data.items():
            entry = KanjiEntry.from_json(entry_data)
            assert entry.kanji not in db
            db[entry.kanji] = entry

        return db


def import_meaning():
    with open(meaning_path) as db_file:  
        data = json.load(db_file)

        return data

def export_db(db):
    data = {}
    for kanji, entry in db.items():
        data[kanji] = entry.to_json()

    with open(db_path, 'w') as db_file:
        json.dump(data, db_file, sort_keys=True, indent=4, ensure_ascii=False)

def main():
    print("root_path: {}".format(root_path))
    print("vocabulary_path: {}".format(vocabulary_path))
    print("current_path: {}".format(current_path))
    print("db_path: {}".format(db_path))

    vocabulary = import_vocabulary()
    print("import {} vocabulary entries.".format(len(vocabulary)))
    current_db = import_current()
    print("import {} kanji entries from the deck.".format(len(current_db)))
    meaning_db = import_meaning()
    print("import {} meaning database.".format(len(meaning_db)))
    db = import_db()
    print("import {} kanji entries from the database.".format(len(db)))

    update_db(vocabulary, current_db, meaning_db, db)

    generate_deck(db)

def update_db(vocabulary, current_db, meaning_db, db):
    updated_kanji = set()
    updated_voc = set()
    # First merge from deck to database
    for kanji, current_entry in current_db.items():
        if kanji not in db:
            db[kanji] = current_entry
            updated_kanji.add(kanji)
        else:
            old_entry = db[kanji]
            if old_entry.note != current_entry.note:
                old_entry.note = current_entry.note
                updated_kanji.add(kanji)
            if old_entry.meaning != current_entry.meaning:
                old_entry.meaning = current_entry.meaning
                updated_kanji.add(kanji)
            for voc_kanji, current_voc in current_entry.vocabulary.items():
                if voc_kanji not in old_entry.vocabulary:
                    old_entry.vocabulary[voc_kanji] = current_voc
                    updated_kanji.add(voc_kanji)
                else:
                    if old_entry.vocabulary[voc_kanji].furigana != current_voc.furigana:
                        old_entry.vocabulary[voc_kanji].furigana = current_voc.furigana
                        updated_kanji.add(kanji)
                        updated_voc.add(voc_kanji)

                    if old_entry.vocabulary[voc_kanji].translation != current_voc.translation:
                        old_entry.vocabulary[voc_kanji].translation = current_voc.translation
                        updated_kanji.add(kanji)
                        updated_voc.add(voc_kanji)

    # Second, add new vocabulary
    for voc in vocabulary:
        for k in voc.kanji:
            if k in kanji_all: # filter only joyo kanji
                if k not in db:
                    entry = KanjiEntry(k)
                    entry.vocabulary[voc.kanji] = voc
                    assert k in meaning_db, "The kanji {} is not in the meaning database".format(voc.kanji)
                    entry.meaning = meaning_db[k]
                    db[k] = entry
                    updated_kanji.add(k)
                else:
                    entry = db[k]
                    if voc.kanji not in entry.vocabulary:
                        entry.vocabulary[voc.kanji] = voc
                        updated_kanji.add(k)
                    else:
                        old_voc = entry.vocabulary[voc.kanji]
                        if old_voc.furigana != voc.furigana:
                            old_voc.furigana = voc.furigana
                            updated_kanji.add(k)
                            updated_voc.add(voc.kanji)
                        if old_voc.translation != voc.translation:
                            old_voc.translation = voc.translation
                            updated_kanji.add(k)
                            updated_voc.add(voc.kanji)

    export_db(db)

    print("there are {} kanjis in the database".format(len(db)))
    print("{} kanji entries have been created or updated: {}".format(len(updated_kanji), json.dumps(list(updated_kanji), ensure_ascii=False)))
    print("{} vocabulary entries have been updated: {}".format(len(updated_voc), json.dumps(list(updated_voc), ensure_ascii=False)))

def generate_deck(db):
    kanji_and_voc = genanki.Model(
        note_type_id,
        'KanjiAndVoc',
        fields=[
            {'name': 'Kanji'},
            {'name': 'Meaning'},
            {'name': 'Vocabulary'},
            {'name': 'Note'},
        ],
        css=card_template_css,
        templates=[
            {
            'name': 'Find Kanji',
            'qfmt': card_template_find,
            'afmt': card_template_answer,
            },
            {
            'name': 'Read Kanji',
            'qfmt': card_template_read,
            'afmt': card_template_answer,
            }
        ])

    deck = genanki.Deck(deck_id, 'Vocabulary Kanji')

    entry_list = sorted(db.values(), key=lambda x: x.joyo_index)
    for entry in entry_list:
        deck.add_note(KanjiNote(model=kanji_and_voc, fields=[
            entry.kanji,
            entry.meaning,
            ';'.join([x.to_line() for k, x in entry.vocabulary.items()]),
            entry.note
        ]))

    genanki.Package(deck).write_to_file(output_deck_path)

    print("Deck has been generated")

if __name__ == '__main__':
    main()