import os
import json
import sqlite3

global root_path
global kanji_sqlite_path
global kanji_damage_path
global meaning_db_path

root_path = os.path.realpath(__file__)
root_path = os.path.abspath(os.path.join(os.path.dirname(root_path), os.pardir))
kanji_sqlite_path = os.path.join(root_path, "data/kanjidb.sqlite")
kanji_damage_path = os.path.join(root_path, "data/kanjidamage.txt")
meaning_db_path = os.path.join(root_path, "data/meanings.txt")

db = {}

with open(kanji_damage_path, 'r') as file:
    for line in file:
        items = line.split('\t')
        assert(len(items) >= 3)

        if len(items[1]) > 1:
            continue # skip line which are not kanjis

        assert(items[1] not in db)
        db[items[1]] = items[2]

conn = sqlite3.connect(kanji_sqlite_path)
cursor = conn.cursor()
cursor.execute("SELECT kanji, meaning FROM kanjidict")
data = cursor.fetchall()

for row in data:
    if row[0] not in db:
        db[row[0]] = row[1]
        
with open(meaning_db_path, 'w') as file:
    json.dump(db, file, ensure_ascii=False, indent=2)

print ("generation done.")